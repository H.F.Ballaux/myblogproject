<?php

session_name('MyBlogProject'.date('y-m-d'));
session_start(['cookie_lifetime'=> 3600]);


require_once 'config.php';
require_once 'lib/pdo.php';
require_once 'lib/output.php';
require_once 'lib/user.php';
require_once 'lib/articles.php';

$connect = connect();

require_once 'structure/header.html';

if (!empty($_SESSION['alert'])){
    if (!empty($_SESSION['alert-color']) && in_array($_SESSION['alert-color'], ['danger', 'info', 'warning', 'success'])){
        $alertColor = $_SESSION['alert-color'];
        unset($_SESSION['alert-color']);
    } else { $alertColor = 'danger'; }
    echo '<div class="alert-'.$alertColor.'">'.$_SESSION['alert'].'</div>';
    unset($_SESSION['alert']);
}

require_once 'structure/topbar.html';
if (empty($_SESSION['userid'])){
    echo '<h1><a style="text-decoration: none" href="index.php?view=view/accueil">MyBlog</a></h1>
        <a href="index.php?view=view/login">Se Connecter</a>';
} else {
    echo '<h1><a style="text-decoration: none" href="index.php?view=view/articles&profil='.getDataFromTable('user', 'u_id', $_SESSION['userid'])->u_pseudo.'">MyBlog</a></h1>
        <a href="index.php?view=view/logout">Se Deconnecter</a>';
}

require_once 'structure/content.html';
if (!empty($_GET['view'])){
    getContent($_GET['view']);
} else {
    getContent('view/accueil');
}

require_once 'structure/footer.html';
