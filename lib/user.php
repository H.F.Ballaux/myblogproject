<?php

function update_image(string $user_id, string $imgPath): bool {
    global $connect;
    $sql = '
        update user 
        set u_photo = ? 
        where u_id = ?
    ';
    $param = [$imgPath, $user_id];
    $query = $connect->prepare($sql);
    $query->execute($param);

    if ($query->rowCount()){
        return true;
    } else {
        return false;
    }
}