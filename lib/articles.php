<?php

function update_image_article(string $art_id,string $imgBasePath): void {
    global $connect;
    $sql = 'update articles set ar_image = ? where ar_id = ?';
    $param = [$imgBasePath, $art_id];
    $query = $connect->prepare($sql);
    $query->execute($param);
}
