<?php


function connect(){
    global $connect;
    if (!is_a($connect, 'PDO')){
        try {
            $connect = new PDO('mysql:dbname='.DB_NAME.'; host='.DB_HOST.';', DB_USER, DB_PWD);
        } catch (PDOException $e){
            die('Error '.$e->getMessage());
        }
    }
    return $connect;
}