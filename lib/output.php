<?php

function getContent(string $file) : void {
    if (is_array(FILES_EXTENSIONS)){
        foreach (FILES_EXTENSIONS as $ext){
            $filename = $file.$ext;
            if (file_exists($filename)){
                include $filename;
            }
        }
    }
}


function getDataFromTable(string $table, string $field, mixed $value): object|bool
{
    if (in_array($table, getTables())){
        if (in_array($field, getColumns($table))){
            global $connect;
            $sql = 'select * from '.$table.' where '.$field.' = ?';
            $param = [$value];
            $query = $connect->prepare($sql);
            $query->execute($param);
            return $query->fetchObject();
        }
    }
    return false;
}

function getColumns(string $table): false|array {
    if (in_array($table, getTables())){
        global $connect;
        $sql = 'describe '.$table;
        $query = $connect->prepare($sql);
        $query->execute();
        $result =  $query->fetchAll(PDO::FETCH_OBJ);
        $tab_res = [];
        foreach ($result as $item){
            $tab_res[] = $item->Field;
        }
        return $tab_res;
    }
    return false;
}

function getTables(): false|array {
    global $connect;
    $sql = 'show tables';
    $query = $connect->prepare($sql);
    $query->execute();
    $result = $query->fetchAll(PDO::FETCH_NUM);
    $tab = [];
    foreach ($result as $item) {
        $tab[] = $item[0];
    }
    return $tab;
}


function getArticles(int $userid): array {
    global $connect;
    $sql = ' select * from articles where ar_user = ? ';
    $param = [$userid];
    $query = $connect->prepare($sql);
    $query->execute($param);

    return $query->fetchAll(PDO::FETCH_OBJ);
}

function getCountComments(int $article_id): object {
    global $connect;
    $sql = 'select count(*) as cpt from comments where c_article = ?';
    $param = [$article_id];
    $query = $connect->prepare($sql);
    $query->execute($param);

    return $query->fetchObject();
}


function getLastItems(string $table): false|array {
    if (in_array($table, getTables())){
        $field = ($table == 'user') ? 'u_inscription' : 'ar_creation';

        global $connect;
        $sql = 'select * from '.$table.' order by '.$field.' desc limit 5';
        $query = $connect->prepare($sql);
        $query->execute();

        return $query->fetchAll(PDO::FETCH_OBJ);
    }
    return false;
}


function transform_date(string $valueDate): string
{
    return date_format(new DateTime($valueDate), 'd/m/y H\hi');
}

function setAlert(string $msg, string $url, string $type = 'danger') :void {
    $_SESSION['alert'] = $msg;
    $_SESSION['alert-color'] = $type;
    header('location:'.$url);
    die;
}