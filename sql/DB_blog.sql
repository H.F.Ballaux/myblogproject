
-- Creation de la DB : DB_blog --

drop database if exists DB_Blog;
create database if not exists DB_Blog;
use DB_Blog;


-- Structure de la table user --

drop table if exists user;
create table if not exists user
(
    u_id int(10) unsigned not null auto_increment ,
    u_pseudo varchar(255) unique not null ,
    u_pwd varchar(255)       not null ,
    u_mail varchar(255)   unique not null ,
    u_dateNaiss date        null ,
    u_description varchar(255) null ,
    u_inscription datetime  not null ,
    u_connexion datetime    null ,
    u_photo varchar(50)     null ,
    primary key (u_id),
    unique key (u_pseudo),
    unique key (u_mail)
) ENGINE innoDB;


-- Structure de la table amis --

drop table if exists amis; -- drop = jetter --
create table if not exists amis -- create = créer --
(
    am_id int(10) unsigned not null auto_increment,
    am_user1 int(10) unsigned not null ,
    am_user2 int(10) unsigned not null ,
    primary key (am_id),
    foreign key (am_user1) references user (u_id),
    foreign key (am_user2) references user (u_id)
) ENGINE innoDB;


-- structure de la table articles --

drop table if exists articles;
create table if not exists articles
(
    ar_id int(10) unsigned not null auto_increment,
    ar_titre varchar(150) not null,
    ar_texte text not null ,
    ar_image varchar(50) null ,
    ar_like int(100) not null default 0,
    ar_user int(10) unsigned not null ,
    ar_creation datetime not null ,
    ar_modifie datetime null ,
    ar_publie bool not null default 0,
    primary key (ar_id),
    foreign key (ar_user) references user (u_id)
) ENGINE innoDB;

drop table if exists likes;
create table if not exists likes(
    l_id int(10) unsigned not null auto_increment,
    l_article int(10) unsigned not null ,
    l_user int(10) unsigned not null ,
    primary key (l_id),
    foreign key (l_article) references articles (ar_id),
    foreign key (l_user) references user (u_id)
) engine InnoDB;

-- trigger sur table likes, permet d'incrémenter le compteur de like sur la table article pour l'article concerné

delimiter |
drop trigger if exists ai_likes;
create trigger ai_likes after insert on likes for each row
    begin
        declare article int;
        set article = NEW.l_article;
        update articles set ar_like = ar_like+1
        where ar_id = article;
    end |
delimiter ;

-- structure de la table commentaires --

drop table if exists comments;
create table if not exists comments(
    c_id int(10) unsigned not null auto_increment,
    c_com text not null ,
    c_user int(10) unsigned not null ,
    c_article int(10) unsigned not null ,
    primary key (c_id),
    foreign key (c_user) references user (u_id),
    foreign key (c_article) references articles (ar_id)
) ENGINE innoDB;

-- zone test --

select count(*) as cpt
from user inner join amis a on user.u_id = a.am_user1
where u_id = 1;

select count(*) as cpt
from articles a  inner join comments c on a.ar_id = c.c_article
where ar_user = 1;