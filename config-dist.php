<?php

const ROOT_PATH = __DIR__;
const FILES_EXTENSIONS = ['.html', '.php'];
const PICTURES_EXTENSIONS = ['image/png', 'image/jpeg', 'image/jpg'];

const DB_NAME = '';
const DB_HOST = '';
const DB_USER = '';
const DB_PWD = '';
