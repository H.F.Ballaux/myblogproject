<?php
$msg = '-- Nouvel Article -- <br>';
$url = 'index.php?view=view/';

if (!empty($_SESSION['userid'])){
    $user = getDataFromTable('user', 'u_id', $_SESSION['userid']);
    if (!empty($_POST['titre']) && !empty($_POST['texte'])){
        $publie = !empty($_POST['publish']) ? 1 : 0;

        global $connect;
        $sql = '
            insert into articles (ar_titre, ar_texte, ar_user, ar_creation, ar_publie)
            VALUES (?,?,?, now(),?)
        ';
        $param = [$_POST['titre'], $_POST['texte'], $user->u_id, $publie];
        $query = $connect->prepare($sql);
        $query->execute($param);

        if ($query->rowCount()){
            $art_id = $connect->lastInsertId();
            $msg.= 'L\'article a bien été ajouté';
            if (!empty($_FILES['photo']['name'] && !empty($_FILES['photo']['tmp_name']))){
                if (in_array($_FILES['photo']['type'], PICTURES_EXTENSIONS )){
                    if ($_FILES['photo']['size']<=2048000){
                        $imgPath = ROOT_PATH.'\\img\\articles\\';
                        if (!is_dir($imgPath.$user->u_id)){
                            mkdir($imgPath.$user->u_id, 755);
                        }
                        $basename = $user->u_id.'-'.$art_id.'.'.explode('/',$_FILES['photo']['type'])[1];
                        $move = move_uploaded_file($_FILES['photo']['tmp_name'], $imgPath.$user->u_id.'\\'.$basename);
                        $basePath = 'img\\articles\\'.$user->u_id.'\\'.$basename;

                        if ($move){
                            update_image_article($art_id, $basePath);
                            $msg .= '<br> L\'image a bien été ajoutée <br>';
                        } else {
                            $msg .= '<br> L\'image n\'a pas été ajoutée <br>';
                        }
                    } else {
                        $msg .= '<br> L\'image est trop grande! <br>';
                    }
                } else {
                    $msg .= '<br> Le format n\'est pas compatible <br>';
                }
            } else {
                $msg .= '<br> Aucune image ajoutée <br>';
            }
            setAlert($msg, $url.'articles&profil='.$user->u_pseudo, 'success');
        } else {
            setAlert($msg.'ca va pas', $url.'newArticle', 'warning');
        }

    } else {
        $msg = 'Il manque <br>';
        if (empty($_POST['titre'])){
            $msg.= 'Le titre';
        }
        if (empty($_POST['texte'])){
            $msg.= '<br>Le texte<br>';
        }
        setAlert($msg, $url.'newArticle', 'info');
    }
} else {
    setAlert($msg.'Vous ne devriez pas etre la', $url.'login', 'info');
}