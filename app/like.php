<?php

function verify_liked_article(int $userid, int $artid): bool {
    global $connect;
    $sql = 'select * from likes where l_article = ? and l_user = ?;';
    $param = [$artid, $userid];
    $query = $connect->prepare($sql);
    $query->execute($param);

    if ($query->rowCount()){
        return true;
    } else {
        return false;
    }
}

function update_likes(mixed $userid, mixed $artid): bool {
    global $connect;
    $sql = 'insert into likes (l_article, l_user) values (?,?)';
    $param = [$artid, $userid];
    $query = $connect->prepare($sql);
    $query->execute($param);

    if ($query->rowCount()){
        return true;
    } else {
        return false;
    }
}

$msg = '-- Like --<br>';
$url = 'index.php?view=view/articles&profil=';

if (!empty($_SESSION['userid'])){
    $user = getDataFromTable('user', 'u_id', $_SESSION['userid']);
    if (!empty($_GET['artid'])){
        $article = getDataFromTable('articles', 'ar_id', $_GET['artid']);
        $auteurArticle = getDataFromTable('user', 'u_id', $article->ar_user)->u_pseudo;
        if (!verify_liked_article($user->u_id, $article->ar_id)){
            update_likes($_SESSION['userid'], $_GET['artid']);
            setAlert($msg.'Votre like a été ajouté', $url.$auteurArticle, 'success');
        } else {
            setAlert($msg.'Vous avez déja liké cet article', $url.$auteurArticle, 'info');
        }
    } else {
        setAlert($msg.'Aucun article sélectioné', $url.$user->u_pseudo);
    }
} else {
    setAlert($msg.'Il faut se connecter pour liker', $url.$_GET['profil']);
}