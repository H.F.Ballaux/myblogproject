<?php

$msg = '-- Login --<br>';
$url = 'index.php?view=view/';

function update_lastLogin(string $user_id): bool {
    global $connect;
    $sql = 'update user set u_connexion = now() where u_id = ?';
    $param = [$user_id];
    $query = $connect->prepare($sql);
    $query->execute($param);

    if ($query->rowCount()){
        return true;
    } else {
        return false;
    }
}

if (!empty($_POST['login']) && !empty($_POST['pwd'])){
    $user = getDataFromTable('user', 'u_pseudo', $_POST['login']);
    if (!empty($user)){
        if (password_verify($_POST['pwd'], $user->u_pwd)){
            $_SESSION['userid'] = $user->u_id;
            update_lastLogin($user->u_id);
            setAlert($msg.'Bienvenue - '.$user->u_pseudo, $url.'articles&profil='.$user->u_pseudo, 'success');
        } else {
            setAlert($msg.' Mots de passe invalide', $url.'login');
        }
    } else {
        setAlert($msg.' Login invalide', $url.'login');
    }
} else {
    $msg .= 'Il manque <br>';
    if (empty($_POST['login'])){
        $msg .= 'Login <br>';
    }
    if (empty($_POST['pwd'])){
        $msg .= 'Mots de passe <br>';
    }
    setAlert($msg, $url.'login', 'info');
}