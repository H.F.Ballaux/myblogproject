<?php


$msg = '-- Inscription --<br>';
$url = 'index.php?view=view/';

if (!empty($_POST['login']) && !empty($_POST['pwd']) && filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
    if (!getDataFromTable('user', 'u_pseudo', $_POST['login'])) {
        if (!getDataFromTable('user', 'u_mail', $_POST['email'])){
            global $connect;
            $sql = '
                insert into user (u_pseudo, u_pwd, u_mail, u_inscription) 
                VALUES (?, ?, ?, now())
            ';
            $params = [trim($_POST['login']), password_hash($_POST['pwd'], PASSWORD_DEFAULT), $_POST['email']];
            $query = $connect->prepare($sql);
            $query->execute($params);

            if ($query->rowCount()){
                $new_user_id = $connect->lastInsertId();
                $msg .= $_POST['login'].' a bien été inscrit';
                $_SESSION['alert-color'] = 'success';

                if (!empty($_FILES['photo']['name']) && !empty($_FILES['photo']['tmp_name'])){
                    if (in_array($_FILES['photo']['type'], PICTURES_EXTENSIONS)){
                        if ($_FILES['photo']['size'] <= 1024000){
                            $imgPath = ROOT_PATH.'\\img\\profil\\';
                            if (!is_dir($imgPath.$new_user_id)){
                                mkdir($imgPath.$new_user_id, 755);
                            }
                            $baseName = $new_user_id.'.'.explode('/', $_FILES['photo']['type'])[1];
                            $move = move_uploaded_file($_FILES['photo']['tmp_name'], $imgPath.$new_user_id.'/'.$baseName);
                            $imgBasePath = 'img\\profil\\'.$new_user_id.'\\'.$baseName;
                            if ($move){
                                update_image($new_user_id, $imgBasePath);
                                $msg .= '<br> L\'image a bien été ajoutée <br>';
                            } else {
                                $msg .= '<br> L\'image n\'a pas été ajoutée <br>';
                            }
                        } else {
                            $msg .= '<br> L\'image est trop grande! <br>';
                        }
                    } else {
                        $msg .= '<br> Le format n\'est pas compatible <br>';
                    }
                } else {
                    $msg .= '<br> Aucune image ajoutée <br>';
                }
                setAlert($msg, $url.'login', 'success');
            } else {
                setAlert($msg . ' ca n\'a pas marché ', $url.'inscription');
            }

        } else {
            setAlert($msg.$_POST['email'].' a déja été utilisé', $url.'login', 'info');
        }
    }else {
        setAlert($msg.$_POST['login'].' a déja été utilisé', $url.'login', 'info');
    }
} else {
    $msg .= 'Il manque<br>';
    if (empty($_POST['username'])) {
        $msg .= 'Le login<br>';
    }
    if (empty($_POST['pwd'])) {
        $msg .= 'Mots de passe<br>';
    }
    if (empty($_POST['email'])) {
        $msg .= 'L\'email';
    }
    setAlert($msg, $url . 'inscription', 'info');
}
