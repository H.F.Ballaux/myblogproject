<?php

$output = '';

function getNbrComments($u_id)
{
    global $connect;
    $sql = '
        select count(*) as compteur
        from articles a inner join comments c on a.ar_id = c.c_article
        where ar_user = ?';
    $param = [$u_id];
    $query = $connect->prepare($sql);
    $query->execute($param);

    return $query->fetchColumn();
}
function getNbrAmis($u_id)
{
    global $connect;
    $sql = '
        select count(*) as compteur
        from user inner join amis on user.u_id = amis.am_user1
        where u_id = ?';
    $param = [$u_id];
    $query = $connect->prepare($sql);
    $query->execute($param);

    return $query->fetchColumn();
}

if (!empty($_GET['profil'])){
    $profil = getDataFromTable('user', 'u_pseudo', $_GET['profil']);


    if (!empty($profil)){
        $_SESSION['profileVisited'] = '&profil='.$profil->u_pseudo;
        $articles = getArticles($profil->u_id);
        rsort($articles);
        $pages = sizeof($articles)/5;
        if (!empty($profil->u_photo)){
            $picture = $profil->u_photo;
        } else {
            $picture = 'img/profil/default/default.png';
        }
        $output.='
        <div class="content_grid">
            <div class="content_grid-01">
                <a href="#">'.$profil->u_pseudo.'</a>
                <img class="profilPicture" src="'.$picture.'" alt="Photos de profil">
                <p><span>Description :</span><br>'.$profil->u_description.'</p>
                <p>Date d\'inscription: '.transform_date($profil->u_inscription) .'</p>
            </div>
            <div class="content_grid-02">
        ';
        if (!empty($_SESSION['userid']) && $profil->u_id == $_SESSION['userid']){
            $output.='
            <div class="userMenu" ">
                <nav>
                    <a href="index.php?view=view/newArticle">Creer articles</a>
                    <a href="index.php?view=view/profil">Mon profil</a>
                    <a href="index.php?view=view/accueil">Accueil</a>
                </nav>
            </div>
            ';
        }
        if (!empty($articles)){
            foreach ($articles as $article){
                $cont_comment = getCountComments($article->ar_id);
                $output.='
                <div class="article">
                    <div class="ar_titre">'.$article->ar_titre.'</div>
                    <div class="ar_main">
                        <div class="img">
                            <img src="'.$article->ar_image.'" alt="">
                        </div>
                        <div class="text">
                            <p>'.$article->ar_texte.'</p>
                        </div>
                    </div>
                    <div class="ar_foot">
                        <div class="link">
                            <a href="index.php?view=app/like&profil='.$_GET['profil'].'&artid='.$article->ar_id.'" class="like">like ['.$article->ar_like.']</a> |
                            <a href="#" class="like">comment ['.$cont_comment->cpt.']</a> |
                            <a href="#" class="like">share</a>
                        </div>
                        <p> '.transform_date($article->ar_creation).' </p>
                    </div>
                </div>
                ';
            }
            $output.='
                <div class="linkPages">
                    <p>
            ';
            for ($i = 1; $i <= $pages; $i++){
                $output.= '<a href="">'.$i.'</a>';
                if ($i<$pages){
                    $output.=' | ';
                }
            }
            $output.=' </div> ';
        } else {
            $output.= 'Il semblerait qu\'il n\'y ai pas d\'articles' ;
        }
        $output .='
        </div>
        <div class="content_grid-03"> 
            <div class="info">
                <p>Info</p>
                <p>Création: '.transform_date($profil->u_inscription).'
                <br>'.getNbrAmis($profil->u_id).' amis
                <br>'.sizeof($articles).' articles
                <br>'.getNbrComments($profil->u_id).' commentaires
                </p>
            </div>
        </div>
    </div>
            ';
    } else {
        echo 'le Profil recherché n\'existe pas ';
    }
}

echo $output;






