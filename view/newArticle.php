<?php

$output = '
<div class="content_form">
    <h2>Nouvel Article</h2>
    <form action="index.php?view=app/newArticle" method="post" enctype="multipart/form-data">
        <label for="titre">Titre</label><br>
        <input type="text" name="titre" id="titre" placeholder="Titre"><br>
        <label for="texte">Article</label><br>
        <textarea name="texte" id="texte" placeholder="Ton Article..." rows="10"></textarea> <br>
        <label for="img"></label>Photo<br>
        <input type="file" id="photo" name="photo" accept="image/jpeg, image/png, image/jpg" value="Photo"><br>
        <label for="publish">Publier</label>
        <input type="checkbox" name="publish" id="publish"><br>
        <input type="submit" value="Finir">
    </form>
</div>

';

echo $output;
