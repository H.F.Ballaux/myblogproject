<?php


$lastUser = getLastItems('user');
$lastArticles = getLastItems('articles');

$output = '
<div class="content_accueil">
    <h2>Accueil</h2>
    <div class="ac_users">
        <h3>Les petits nouveaux</h3>
        <div class="ac_us_flex">
';
foreach ($lastUser as $item){
    $photo = (!empty($item->u_photo)) ? $item->u_photo : 'img/profil/default/default.png';
    $output .= '
                <a href="index.php?view=view/articles&profil='.$item->u_pseudo.'">
                    <img class="profile" src="'.$photo.'" alt=""><br>
                    <p>'.$item->u_pseudo.'</p>
                </a>
                ';
}
$output .= '
        </div>
    </div>
    <div class="ac_articles">
        <h3>Les derniers articles</h3>
        <div class="ac_art_flex">
';

foreach ($lastArticles as $item){
    $auteur = getDataFromTable('user', 'u_id', $item->ar_user);
    $ar_modifie = (!empty($item->ar_modifie)) ? transform_date($item->ar_modifie) : '/';
    $ar_image = (!empty($item->ar_image))? '<img src="'.$item->ar_image.'" alt=""><br>' : '';
    $output .= '
                <div class="ac_art_article">
                    <legend>'.$item->ar_titre.'</legend>
                    '.$ar_image.'
                    <p>Extrait : '.substr($item->ar_texte, 0, 20).'...<br>
                    Cree le : '.transform_date($item->ar_creation) .'<br>
                    modifie le : '.$ar_modifie.'<br>
                    Auteur : '.$auteur->u_pseudo.'
                    </p>
                </div>
                ';
}

$output .= '
    </div>
</div>
';

echo $output;
